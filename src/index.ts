export { TomTom } from "./Tomtom";
export { Coordinates, Store, DistanceStore, GeoData} from "./types";