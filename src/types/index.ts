interface Address {
    id: string;
    street: string;
    district: string;
    number: number;
    coordinate: string;
    city: string;
    state: string;
    cep: string;
}
export interface Store {
    store_id: string;
    categoryId: string;
    storeCategories?: Object;
    address: Address;
    addressId: string;
    user: Object;
    name: string;
}
export interface GeoData {
    street: string;
    number: number;
    district: string;
    city: string;
    state: string;
    cep: string;
}
export interface DistanceStore {
    id: string;
    name: string;
    lengthInMeters: number;
}
export interface Coordinates {
    coordinates: string;
}
export {};
