import axios, {AxiosInstance} from "axios";
import { Coordinates, GeoData, Store, DistanceStore} from "./types";



export class TomTom{

    baseURL: string = "https://api.tomtom.com/";
    apiKey: string;
    AxiosInstance: AxiosInstance;

    constructor(apiKey: string) {
        this.AxiosInstance = axios.create({
            baseURL: this.baseURL
        });
        this.apiKey = apiKey;
    };

    async geocoding(geoData: GeoData) {
        const adress = `${geoData['street']}, ${geoData['number']}-${geoData['district']},${geoData['city']} - ${geoData['state']}, ${geoData['cep']}`;
        let coordinates: any;

        try {
            const { data } = await this.AxiosInstance.get(
                `${this.baseURL}search/2/geocode/${adress}.json?lat=37.337&lon=-121.89&key=${this.apiKey}`
                );            
            coordinates = data['results'][0]['position'];
    
        } catch(err) {            
            if(axios.isAxiosError(err)) {
                return err.message;

            };
        };
        return {coordinates: `${coordinates['lat']},${coordinates['lon']}`} as Coordinates;

    };

    async calculateRoute(userCoordinates: string, listOfStores: Array<Store>) {
        let storesArray = [];

        try{
            for(let i = 0; i < listOfStores.length; i++) {            
                const { data } = await this.AxiosInstance.get(`${this.baseURL}routing/1/calculateRoute//${userCoordinates}:${listOfStores[i].address.coordinate}/json?instructionsType=text&language=en-US&vehicleHeading=90&sectionType=traffic&routeType=eco&traffic=true&avoid=unpavedRoads&travelMode=motorcycle&vehicleMaxSpeed=120&vehicleCommercial=false&vehicleEngineType=combustion&key=${this.apiKey}`);
                
                const lengthInMeters = data['routes'][0]['legs'][0]['summary']['lengthInMeters'];
                const store = {
                    id: listOfStores[i].store_id,
                    name: listOfStores[i].name,
                    lengthInMeters: lengthInMeters
                };
                storesArray.push(store);
            };

        } catch(err) {
            if(axios.isAxiosError(err)) {
                return err.message;
            };  
        };
        storesArray.sort((a, b) => {
            return a.lengthInMeters - b.lengthInMeters;
          });

        return storesArray as DistanceStore[];

    };
};
